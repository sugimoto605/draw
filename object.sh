#object.sh
#	draw 3D object by draw.sh
#	Tue Aug 18 16:50:49 JST 2020	Version 1.0	Using z-buffer
#	Fri Aug 21 10:52:16 JST 2020	Version	1.1	PlaneSort
#	Fri Aug 21 15:40:40 JST 2020	Version	1.2	LineSplitter
#	Mon Aug 24 15:05:26 JST 2020	Version	1.3	arc -- still produces some error omg
#	Wed Nov 25 13:09:26 JST 2020	Version 1.4 OFF & PS3 DAE format
#	Thu Jul 22 11:42:24 JST 2021	Version 1.5 Draw triangle/tetragon
#---input file format:
#	1	1	1	width		<--	define width
#	2	3	2	cube		<-- draw cube of width at the position
#	1	0	2	vector		<--	define vector
#	1	1	0	width
#	5	5	5	parallel	<-- draw parallelpiped parallel to vector at the position
#								base plane is identified by width=0
#---make 2D projection
#	geometry_projection azimuth zenith	< inpput > output
#	The polar direction is Z-axis by default. You can use Y-axis by
#	pole 	    < input |geometry_projection azimuth zenith	> output
#	X-axis by
#	(pole|pole) < input |geometry_projection azimuth zenith	> output
#---draw it
#	use OPT="-q 0.5 -N x -N y" in draw.sh
#----------------------
projection(){
	#projection theta phi (degree)
	#Read (x,y,z) and map to 2D plane
	gawk -v theta_camera=$1 -v phi_camera=$2 'BEGIN{
		eps=1e-10;
		#---Prepare (n,t,s) vectors
		pi=4*atan2(1,1);
		theta_camera=theta_camera/180*pi;
		phi_camera=phi_camera/180*pi;
		if (phi_camera==0)
		{
			s[0]=0;
			s[1]=1;
			s[2]=0;
			t[0]=-1;
			t[1]=0;
			t[2]=0;
		}
		else
		{
			#View Camera
			n[0]=sin(phi_camera)*cos(theta_camera);
			n[1]=sin(phi_camera)*sin(theta_camera);
			n[2]=cos(phi_camera);
			printf("#<vectors>\n");
			printf("# n vector= %f %f %f\n",n[0],n[1],n[2]);
			#projection makes X in R3 space into R2 space
			#	vector X --->  (n.X,s.X,t.X)	--->	(s.X,t.X)
			#unit normal n is specified by user.
			#	n[0]*s[0]+n[1]*s[1]+n[2]*s[2]=0
			#	s[0]*t[0]+s[1]*t[1]+s[2]*t[2]=0
			#	n[0]*t[0]+n[1]*t[1]+n[2]*t[2]=0
			#The above equation defines s and t except 1 parameter
			#which is defined by [ projection of Z axis looks vertical direction ]
			#	(0,0,1)	-->	( (s.(0,0,1), t.(0,0,1))	s.(0,0,1)=0
			_tmp=sqrt(n[0]*n[0]+n[1]*n[1]);
			s[0]=-n[1]/_tmp;
			s[1]=+n[0]/_tmp;
			s[2]=0;
			printf("# s vector= %f %f %f\n",s[0],s[1],s[2]);
			t[0]=n[1]*s[2]-n[2]*s[1];
			t[1]=n[2]*s[0]-n[0]*s[2];
			t[2]=n[0]*s[1]-n[1]*s[0];
			printf("# t vector= %f %f %f\n",t[0],t[1],t[2]);
			printf("#</vectors>\n");
			LOGFILE="object.log"
		}
		#---Prepare output stack
		n_data=0;
		C_data[n_data]="";
	}function abs(x){
		if (x>0) return x;
		else return -x;
	}function acos(x){
		return atan2(sqrt(1-x*x),x);
	}function asin(x){
		return atan2(x,sqrt(1-x*x));
	}function dot(x,y){
		return x[0]*y[0]+x[1]*y[1]+x[2]*y[2];
	}function dot2(x,y){
		return x[0]*y[0]+x[1]*y[1];
	}function XV(x,y,z){
		return s[0]*x+s[1]*y+s[2]*z;
	}function YV(x,y,z){
		return t[0]*x+t[1]*y+t[2]*z;
	}function ZV(x,y,z){
		return n[0]*x+n[1]*y+n[2]*z;
	}function normalize(a,	_k){
		_sz=sqrt(dot(a,a));
		if (_sz>eps) for(_k=0;_k<3;_k++) a[_k]=a[_k]/_sz;
		return _sz>eps;
	}function normalize2(a,	_k){
		_sz=sqrt(dot2(a,a));
		if (_sz>eps)
		{
			for(_k=0;_k<2;_k++) a[_k]=a[_k]/_sz;
			return 1;
		}
		else
			return	0;
	}function get_theta(a,n){
		#	a, n must be unit vectors
		return acos(abs(dot(a,n)))/pi*180;
	}function zero(a,	_k){
		for(_k=0;_k<3;_k++) a[_k]=0;
	}function sign(x){
		if (x==0) return 0;
		else if (x>0) return 1;
		else return -1;
	}function cross(nvec,a,b,	_k){
		nvec[0]=a[1]*b[2]-a[2]*b[1];
		nvec[1]=a[2]*b[0]-a[0]*b[2];
		nvec[2]=a[0]*b[1]-a[1]*b[0];
		_sz=sqrt(dot(nvec,nvec));
		if (_sz>eps)
			for(_k=0;_k<3;_k++) nvec[_k]/=_sz;
		return _sz>eps;
	}function _intersect2(tangent,fr,to,		_normal,_sz,_fr,_to,_alpha,_at,_rate){
		#	check if segment (fr[0],fr[1])-(to[0],to[1]) interects (0,0)-(tangent[0],tangent[1])
		_normal[0]=-tangent[1];
		_normal[1]=+tangent[0];
		_sz=dot2(tangent,tangent);
		if (sqrt(_sz)<eps) return -1;
		_fr[0]=dot2(fr,tangent)/_sz;
		_fr[1]=dot2(fr,_normal)/_sz;
		_to[0]=dot2(to,tangent)/_sz;
		_to[1]=dot2(to,_normal)/_sz;
		#	line connecting (_fr[0],_fr[1])-(_to[0],_to[1]): y=(_at1-_at0) x + _at0
		_rate=abs(_to[0]-_fr[0]);
		if (_rate<eps)
		{
			if (_fr[0]<0) return -1;
			if (_fr[0]>1) return -1;
			if (sign(_fr[1])==sign(_to[1])) return -1;
			return _fr[0];
		}
		else
		{
			_alpha=(_to[1]-_fr[1])/(_to[0]-_fr[0]);
			_at[0]=_fr[1]-_alpha*_fr[0];
			_at[1]=_alpha+_at[0];
			if (abs(_at[0])<eps) return	0;	#	intersect at 0
			if (abs(_at[1])<eps) return	1;	#	intersect at 1
			if (sign(_at[0])==sign(_at[1])) return -1;
			#	segments intersects at (return value)*_tangent
			_rate=abs(_at[0]/(_at[1]-_at[0]));
			return _rate;
		}
	}function intersect(pos,s0,s1,t0,t1,		_dim,_k,_tbase,_tang,_c0,_c1){
		_dim=length(s0);
		#	check if line segments (s0[0],s0[1])-(s1[0],s1[1])
		#	and (t0[0],t0[1])-(t1[0],t1[1]) intersects
		#	[1] base vector: t1-t0
		for(_k=0;_k<_dim;_k++)
		{
			_tang[_k]=t1[_k]-t0[_k];
			_c0[_k]=s0[_k]-t0[_k];
			_c1[_k]=s1[_k]-t0[_k];
		}
		if (_intersect2(_tang,_c0,_c1)<-0.5) return 0;
		#	[1] base vector: s1-s0
		for(_k=0;_k<_dim;_k++)
		{
			_tang[_k]=s1[_k]-s0[_k];
			_c0[_k]=t0[_k]-s0[_k];
			_c1[_k]=t1[_k]-s0[_k];
		}
		_tbase=_intersect2(_tang,_c0,_c1);
		if (_tbase<-0.5) return 0;
		#	segments intersects at _tang*_sbase+s0
		for(_k=0;_k<_dim;_k++)
			pos[_k]=_tang[_k]*_tbase+s0[_k];
		return 1;
	}function is_outside(pos,i_top,i_end,	i,__x0,__x1,__t0,__t1,__sth,__sz){
		__sth[0]=0;
		for(i=i_top;i<i_end;i++)
		{
			__x0[0]=X_data[i];		__x0[1]=Y_data[i];
			__x1[0]=X_data[i+1];	__x1[1]=Y_data[i+1];
			__t0[0]=__x0[0]-pos[0];	__t0[1]=__x0[1]-pos[1];
			__t1[0]=__x1[0]-pos[0];	__t1[1]=__x1[1]-pos[1];
			__sz[0]=normalize2(__t0);
			__sz[1]=normalize2(__t1);
			if (__sz[0]&&__sz[1])
			{
				__sth[1]=asin(__t0[0]*__t1[1]-__t0[1]*__t1[0]);
				if (dot2(__t0,__t1)<0)	__sth[1]=sign(__sth[1])*pi-__sth[1];
				__sth[0]=__sth[0]+__sth[1];
			}
			else
			{
				__sth[0]=2*pi;
				break;
			}
		}
		if (abs(__sth[0])<pi/17) return	1;
		return 0;
	}{
		_sta=substr($0,1,1);
		if (_sta=="#")
		{
			if (C_data[n_data]=="") C_data[n_data]=$0;
			else C_data[n_data]=C_data[n_data] "\n" $0;
		}
		else if (NF>0)
		{
			_sta="";
			for(i=4;i<=NF;i++) _sta=_sta " " $i;
			X_data[n_data]=XV($1,$2,$3);
			Y_data[n_data]=YV($1,$2,$3);
			Z_data[n_data]=ZV($1,$2,$3);
			O_data[n_data]=_sta;
			N_data[n_data]=1;
			n_data=n_data+1;
			C_data[n_data]="";
		}
		else
		{
			N_data[n_data-1]=0;
			printf("\n");
		}
	}END{
	#---define objects
		#		X_data[..] Y_data[...]... N_data[]=1	dataset begin
		#		X_data[..] Y_data[...]... N_data[]=1
		#			....
		#		X_data[..] Y_data[...]... N_data[]=0	dataset end
		#		X_data[..] Y_data[...]... N_data[]=1	dataset begin
		#		X_data[..] Y_data[...]... N_data[]=1
		#			....
		#		X_data[..] Y_data[...]... N_data[]=0	dataset end
		n_set=0;
		n_start=0;
		z_sum=0;
		for(i=0;i<n_data;i++)
		{
			z_sum+=Z_data[i];
			if (i==n_start)
			{
				#	start new object
				z_max=Z_data[i];
				t_stage=0;
			}
			else
				if (Z_data[i]>z_max) z_max=Z_data[i];
			if (t_stage==0)
			{
				#first line segment. compute vecA
				vecA[0]=X_data[i]-X_data[n_start];
				vecA[1]=Y_data[i]-Y_data[n_start];
				vecA[2]=Z_data[i]-Z_data[n_start];
				zero(normal);
				if (normalize(vecA))
				{
					t_stage=1;
					#theta_val=get_theta(vecA,n);
				}
			}
			else if (t_stage==1)
			{
				#second line segment. compute vecB
				vecB[0]=X_data[i]-X_data[n_start];
				vecB[1]=Y_data[i]-Y_data[n_start];
				vecB[2]=Z_data[i]-Z_data[n_start];
				if (normalize(vecB))
					if (cross(normal,vecA,vecB))
					{
						t_stage=2;
						#theta_val=get_theta(normal,n);
					}
			}
			else
			{
				#other line segment. replace vecB
				vecB[0]=X_data[i]-X_data[n_start];
				vecB[1]=Y_data[i]-Y_data[n_start];
				vecB[2]=Z_data[i]-Z_data[n_start];
				if (normalize(vecB))
					if (cross(new_normal,vecA,vecB))
					{
						if (cross(new_normal,normal))
						{
							#	new_normal is not parallel to normal
							#	this means that new object started here
							N_data[i-1]=0;
							t_stage--;
							i--;
							break;
						}
						else
						{
							t_stage+=1;
						}
					}
			}
			#	t_stage
			#	1	object consists of 1 line segment
			#	2	object consists of 2 line segment. normal is defined
			#	3..	object consists of t_sgate segments with common normal
			if (N_data[i]==0)
			{
				#	this is the end of the dataset
				n_points=i-n_start+1;
				n_set=n_set+1;
				if (n_points==1) S_kind[n_set]="dot";
				else if (n_points==2) S_kind[n_set]="line";
				else S_kind[n_set]="plane";
				S_top[n_set]=n_start;
				S_end[n_set]=i;
				if (S_kind[n_set]=="plane")
				{
					S_X[n_set]=normal[0];
					S_Y[n_set]=normal[1];
					S_Z[n_set]=normal[2];
					#	Equation is n.x=c
					S_C[n_set]=normal[0]*X_data[n_start]+normal[1]*Y_data[n_start]+normal[2]*Z_data[n_start];
				}
				for(j=n_start;j<=i;j++)
					N_data[j]=n_set;
				n_start=i+1;
				t_stage=0;
			}
		}
		printf("#Found %d objects\n",n_set);
		n_plane=0;
		for(k=1;k<=n_set;k++) if (S_kind[k]=="plane") idx_p[++n_plane]=k;
		n_line=0;
		for(k=1;k<=n_set;k++) if (S_kind[k]=="line") idx_l[++n_line]=k;
	#---sort planes
		for(k=1;k<=n_plane;k++)
			printf("Plane[%3d]%3d %3d:%3d (%9f %9f %9f).x= %9f\n",idx_p[k],k,S_top[idx_p[k]],S_end[idx_p[k]],\
			S_X[idx_p[k]],S_Y[idx_p[k]],S_Z[idx_p[k]],S_C[idx_p[k]]) > LOGFILE;
		sort_plane=1;
		while (sort_plane)
		{
			sort_plane=0;
			for(k=1;k<n_plane;k++) for(m=k+1;m<=n_plane;m++)
			{
				jn	=idx_p[k];
				jp	=idx_p[m];
				#	flag	0:undefined		-1: jn > jp		+1: jn < jp
				flag=0;
				plane_intersect=0;
				#	Skip plane normal to z direction
				if (abs(S_Z[jp])<eps) continue;
				#	Check jp-points in jn-plane
				for(i=S_top[jp];i<S_end[jp];i++)
				{
					s0[0]=X_data[i];	s0[1]=Y_data[i];	s0[2]=Z_data[i];
					if (is_outside(s0,S_top[jn],S_end[jn])) continue;
					#s0 in jn-plane
					n[0]=S_X[jn];		n[1]=S_Y[jn];		n[2]=S_Z[jn];	c=S_C[jn];
					zv=(dot(s0,n)-c)*sign(n[2]);
					if (abs(zv)<eps) continue;
					if (flag==0)
						flag=sign(zv);
					else if (zv*flag<0)
					{
						printf("ERROR:PlaneSort:Plane[%3d][%3d] intersect.\n",jn,jp)	>	LOGFILE;
						plane_intersect=1;
						break;
					}
				}
				if (plane_intersect) continue;
				#	Check jn-points in jp-plane
				for(i=S_top[jn];i<S_end[jn];i++)
				{
					s0[0]=X_data[i];	s0[1]=Y_data[i];	s0[2]=Z_data[i];
					if (is_outside(s0,S_top[jp],S_end[jp])) continue;
					#s0 in jp-plane
					n[0]=S_X[jp];		n[1]=S_Y[jp];		n[2]=S_Z[jp];	c=S_C[jp];
					zv=(dot(s0,n)-c)*sign(n[2]);
					if (abs(zv)<eps) continue;
					if (flag==0)
						flag=-sign(zv);
					else if (zv*flag>0)
					{
						printf("ERROR:PlaneSort:Plane[%3d][%3d] intersect.\n",jn,jp)	>	LOGFILE;
						plane_intersect=1;
						break;
					}
				}
				if (plane_intersect) continue;
				#	Check intersection of jp-sides and jn-sides
				for(i=S_top[jp];i<S_end[jp];i++)
				{
					s0[0]=X_data[i];	s0[1]=Y_data[i];
					s1[0]=X_data[i+1];	s1[1]=Y_data[i+1];
					for(j=S_top[jn];j<S_end[jn];j++)
					{
						t0[0]=X_data[j];	t0[1]=Y_data[j];
						t1[0]=X_data[j+1];	t1[1]=Y_data[j+1];
						if (intersect(pos,s0,s1,t0,t1))
						{
							#	get z value of plane[jp]
							pos[2]=(S_C[jp]-S_X[jp]*pos[0]-S_Y[jp]*pos[1])/S_Z[jp];
							#	check pos[] is closer than plane[jn]
							n[0]=S_X[jn];	n[1]=S_Y[jn];	n[2]=S_Z[jn];	c=S_C[jn];
							zv=(dot(pos,n)-c)*sign(n[2]);
							if (abs(zv)<eps)
								continue;
							if (flag==0)
								flag=sign(zv);
							else if (zv*flag<0)
							{
								printf("ERROR:PlaneSort:Plane[%3d][%3d] intersect.\n",jn,jp)	>	LOGFILE;
								plane_intersect=1;
								break;
							}
						}
					}
					if (plane_intersect) break;
				}
				if (plane_intersect) continue;
				if (flag<0)
				{
					idx_p[k]=jp
					idx_p[m]=jn;
					sort_plane=1;
					printf("PlaneSort:Plane[%d]<-->[%d]\n",jp,jn) > LOGFILE;
				}
			}
		}
		for(k=1;k<=n_plane;k++)
		{
			comment=C_data[S_top[idx_p[k]]];
			gsub("\n",";",comment);
			printf("Plane[%3d]%3d %d:%d %s\n",idx_p[k],k,S_top[idx_p[k]],S_end[idx_p[k]],comment) > LOGFILE
		}
	#---split lines
		for(k=1;k<=n_line;k++)
			printf("Line [%3d]%3d %3d:%3d ( %f %f %f )-( %f %f %f )\n",idx_l[k],k,
				S_top[idx_l[k]],S_end[idx_l[k]],
				X_data[S_top[idx_l[k]]],Y_data[S_top[idx_l[k]]],Z_data[S_top[idx_l[k]]],
				X_data[S_end[idx_l[k]]],Y_data[S_end[idx_l[k]]],Z_data[S_end[idx_l[k]]]) > LOGFILE;
		#	split line when it go across the plane
		for(k=1;k<=n_line;k++)
		{
			kl=idx_l[k];
			kf=S_top[kl];
			fr[0]=X_data[kf];	fr[1]=Y_data[kf];	fr[2]=Z_data[kf];
			for(j=1;j<=n_plane;j++)
			{
				jp=idx_p[j];
				to[0]=X_data[kf+1];	to[1]=Y_data[kf+1];	to[2]=Z_data[kf+1];	#	to[] may modified by other plane
				for(m=0;m<3;m++) t[m]=to[m]-fr[m];
				#	get the cross point
				n[0]=S_X[jp];	n[1]=S_Y[jp];	n[2]=S_Z[jp];	c=S_C[jp];
				sz=dot(n,t);
				if (abs(sz)<eps) continue;
				kval=(c-dot(fr,n))/sz;
				#	check if intersection is in the segment
				if (kval<eps) continue;
				if (kval>1-eps) continue;
				for(m=0;m<3;m++) pos[m]=fr[m]+kval*t[m];
				#	check if intersection is in the plane
				if (is_outside(pos,S_top[jp],S_end[jp])) continue;
				#	create new segment
				n_set++;
				S_top[n_set]=n_data;
				S_end[n_set]=n_data+1;
				X_data[n_data]=pos[0];		X_data[n_data+1]=to[0];		X_data[kf+1]=pos[0];
				Y_data[n_data]=pos[1];		Y_data[n_data+1]=to[1];		Y_data[kf+1]=pos[1];
				Z_data[n_data]=pos[2];		Z_data[n_data+1]=to[2];		Z_data[kf+1]=pos[2];
				C_data[n_data]=C_data[kf];	C_data[n_data+1]=C_data[kf+1];
				O_data[n_data]=O_data[kf];	O_data[n_data+1]=O_data[kf+1];
				N_data[n_data]=n_set;		N_data[n_data+1]=n_set;
				n_data+=2;
				n_line++;
				idx_l[n_line]=n_set;
				printf("LineSplit:Line[%d] from [%d], insert ( %f %f %f ) pass through plane[%d]\n",
					n_set,idx_l[k],pos[0],pos[1],pos[2],jp)	>LOGFILE;
			}
		}
		#	split line when it go across the side
		for(k=1;k<=n_line;k++)
		{
			kl=idx_l[k];
			kf=S_top[kl];
			fr[0]=X_data[kf];	fr[1]=Y_data[kf];	fr[2]=Z_data[kf];
			for(j=1;j<=n_plane;j++)
			{
				jp=idx_p[j];
				to[0]=X_data[kf+1];	to[1]=Y_data[kf+1];	to[2]=Z_data[kf+1];	#	to[] may modified by other plane
				#	Check intersection of jp-sides and line
				for(i=S_top[jp];i<S_end[jp];i++)
				{
					t0[0]=X_data[i];	t0[1]=Y_data[i];
					t1[0]=X_data[i+1];	t1[1]=Y_data[i+1];
					if (intersect(pos,fr,to,t0,t1))
					{
						#	omit zero lengh line
						for(m=0;m<3;m++) t[m]=fr[m]-pos[m];	if (sqrt(dot(t,t))<eps) continue;
						for(m=0;m<3;m++) t[m]=to[m]-pos[m]; if (sqrt(dot(t,t))<eps) continue;
						#	create new segment
						n_set++;
						S_top[n_set]=n_data;
						S_end[n_set]=n_data+1;
						X_data[n_data]=pos[0];		X_data[n_data+1]=to[0];		X_data[kf+1]=pos[0];
						Y_data[n_data]=pos[1];		Y_data[n_data+1]=to[1];		Y_data[kf+1]=pos[1];
						Z_data[n_data]=pos[2];		Z_data[n_data+1]=to[2];		Z_data[kf+1]=pos[2];
						C_data[n_data]=C_data[kf];	C_data[n_data+1]=C_data[kf+1];
						O_data[n_data]=O_data[kf];	O_data[n_data+1]=O_data[kf+1];
						N_data[n_data]=n_set;		N_data[n_data+1]=n_set;
						n_data+=2;
						n_line++;
						idx_l[n_line]=n_set;
						printf("LineSplit:Line[%d] from [%d], insert ( %f %f %f ) across plane[%d]\n",
						n_set,idx_l[k],pos[0],pos[1],pos[2],jp)	>LOGFILE;
					}
				}
			}
		}
		for(k=1;k<=n_line;k++)
		{
			skip_l[k]=0;
			printf("Line [%3d]%3d %3d:%3d ( %f %f %f )-( %f %f %f )\n",idx_l[k],k,
				S_top[idx_l[k]],S_end[idx_l[k]],
				X_data[S_top[idx_l[k]]],Y_data[S_top[idx_l[k]]],Z_data[S_top[idx_l[k]]],
				X_data[S_end[idx_l[k]]],Y_data[S_end[idx_l[k]]],Z_data[S_end[idx_l[k]]]) > LOGFILE;
		}
	#---place lines
		k_obj=0;
		for(k=1;k<=n_plane;k++)
		{
			ip	=idx_p[k];
			n[0]=S_X[ip];	n[1]=S_Y[ip];	n[2]=S_Z[ip];	c=S_C[ip];
			#	output line behind plane[ip]
			for(m=1;m<=n_line;m++)
			{
				if (skip_l[m]) continue;	#	because it is already placed
				iL	=idx_l[m];
				_s0[0]=X_data[S_top[iL]];	_s0[1]=Y_data[S_top[iL]];	_s0[2]=Z_data[S_top[iL]];
				_s1[0]=X_data[S_end[iL]];	_s1[1]=Y_data[S_end[iL]];	_s1[2]=Z_data[S_end[iL]];
				#	if line[m] does not intersect with sides of plane[ip], skip it
				outside=1;
				for(i=S_top[ip];i<S_end[ip];i++)
				{
					_t0[0]=X_data[i];	_t0[1]=Y_data[i];
					_t1[0]=X_data[i+1];	_t1[1]=Y_data[i+1];
					if (intersect(pos,_s0,_s1,_t0,_t1)) outside=0;
				}
				if (outside)
				{
					#	even if there is no intersection, it is possible that segment is in the area
					_f1=is_outside(_s0,S_top[ip],S_end[ip]);
					_f2=is_outside(_s1,S_top[ip],S_end[ip]);
					if (_f1||_f2)
					{
						continue;
					}
				}
				z0	=(dot(_s0,n)-c)*sign(n[2]);
				z1	=(dot(_s1,n)-c)*sign(n[2]);
				if (z0>eps) continue;
				if (z1>eps) continue;
				#	push this segment to buffer
				idx_obj[++k_obj]=iL;
				printf("ObjPlace:line [%d]\n",iL)	>LOGFILE;
				skip_l[m]=1;
			}
			printf("ObjPlace:plate[%d]\n",idx_p[k])		>LOGFILE;
			idx_obj[++k_obj]=idx_p[k];
		}
		for(m=1;m<=n_line;m++)
		{
			if (skip_l[m]) continue;
			idx_obj[++k_obj]=idx_l[m];
			printf("ObjPlace:Flush line [%d]\n",idx_l[m])		>LOGFILE;
			skip_l[m]=1;
		}
	#---output buffer
		for(k=1;k<=k_obj;k++)
		{
			for(i=0;i<n_data;i++)
			{
				if (N_data[i]==idx_obj[k]) 
				{
					if (C_data[i]!="") printf("%s\n",C_data[i]);
					printf("%f %f %s\n",X_data[i],Y_data[i],O_data[i]);
				}
			}
			printf("\n");
		}
	}'
}
poll(){
	gawk '{
		_sta=substr($0,1,1);
		if (_sta=="#") print;
		else if (NF>0)	{
			_sta="";
			for(i=4;i<=NF;i++) _sta=_sta " " $i;
			printf("%e %e %e %s\n",$3,$1,$2,_sta);
		}
		else		printf("\n");
	}'
}
geometry(){
	gawk -v theta_camera=$1 -v phi_camera=$2 'BEGIN{
		pi=4*atan2(1,1);
		if (phi_camera==0) phi_camera=45;
		theta_camera=theta_camera/180*pi;
		phi_camera=phi_camera/180*pi;
		#View Camera
		n[0]=sin(phi_camera)*cos(theta_camera);
		n[1]=sin(phi_camera)*sin(theta_camera);
		n[2]=cos(phi_camera);
		#width default
		size[0]=1;
		size[1]=1;
		size[2]=1;
		#vector default
		vec[0]=1;
		vec[1]=1;
		vec[2]=1;
		alt[0]=1;
		alt[1]=1;
		alt[2]=1;
		rad[0]=1;
		rad[1]=1;
		rad[2]=1;
		theta[0]=0;
		theta[1]=360;
		theta[2]=0;
		comment="";
	}function abs(x){
		if (x>0) return x;
		else return -x;
	}function arc(){
		printf("#arc\n");
		if (comment!="") printf("%s\n",comment);
		arc_r_sz=sqrt(dot(rad,rad));
		arc_v_sz=sqrt(dot(vec,vec));
		if (arc_r_sz<eps) return;
		if (arc_v_sz<eps) return;
		arc_r[0]=rad[0]/arc_r_sz;	arc_r[1]=rad[1]/arc_r_sz;	arc_r[2]=rad[2]/arc_r_sz;
		arc_v[0]=vec[0]/arc_v_sz;	arc_v[1]=vec[1]/arc_v_sz;	arc_v[2]=vec[2]/arc_v_sz;
		arc_n[0]=arc_r[1]*arc_v[2]-arc_r[2]*arc_v[1];
		arc_n[1]=arc_r[2]*arc_v[0]-arc_r[0]*arc_v[2];
		arc_n[2]=arc_r[0]*arc_v[1]-arc_r[1]*arc_v[0];
		arc_n_sz=sqrt(dot(arc_n,arc_n));
		if (arc_n_sz<eps) return;
		arc_t[0]=arc_n[1]*arc_r[2]-arc_n[2]*arc_r[1];
		arc_t[1]=arc_n[2]*arc_r[0]-arc_n[0]*arc_r[2];
		arc_t[2]=arc_n[0]*arc_r[1]-arc_n[1]*arc_r[0];
		arc_dt=(theta[1]-theta[0])/int(theta[1]-theta[0]);
		for(arc_theta=theta[0];arc_theta<=theta[1];arc_theta+=arc_dt)
		{
			arc_rate[0]=cos(arc_theta*pi/180);
			arc_rate[1]=sin(arc_theta*pi/180);
			arc_n[0]=(arc_rate[0]*arc_r[0]+arc_rate[1]*arc_t[0])*arc_r_sz+base[0];
			arc_n[1]=(arc_rate[0]*arc_r[1]+arc_rate[1]*arc_t[1])*arc_r_sz+base[1];
			arc_n[2]=(arc_rate[0]*arc_r[2]+arc_rate[1]*arc_t[2])*arc_r_sz+base[2];
			printf("%f %f %f\n",arc_n[0],arc_n[1],arc_n[2]);
		}
		if (abs(abs(theta[1]-theta[0])-360)>eps) printf("%f %f %f\n\n",base[0],base[1],base[2]);
		else printf("\n");
	}function dot(x,y){
		return x[0]*y[0]+x[1]*y[1]+x[2]*y[2];
	}function line(){
		printf("#line\n");
		if (comment!="") printf("%s\n",comment);
		printf("%f %f %f\n",base[0],base[1],base[2]);
		printf("%f %f %f\n\n",base[0]+size[0],base[1]+size[1],base[2]+size[2]);
	}function plane(base,a,b){
		printf("#plane\n");
		if (comment!="") printf("%s\n",comment);
		printf("%f %f %f\n",base[0],base[1]	,base[2]);
		printf("%f %f %f\n",base[0]+a[0],base[1]+a[1],base[2]+a[2]);
		printf("%f %f %f\n",base[0]+a[0]+b[0],base[1]+a[1]+b[1],base[2]+a[2]+b[2]);
		printf("%f %f %f\n",base[0]+b[0],base[1]+b[1],base[2]+b[2]);
		printf("%f %f %f\n",base[0],base[1]	,base[2]);
		printf("\n");
	}function tetragon(){
		printf("#tetragon\n");
		if (comment!="") printf("%s\n",comment);
		printf("%f %f %f\n",base[0],base[1],base[2]);
		printf("%f %f %f\n",base[0]+size[0],base[1]+size[1],base[2]+size[2]);
		printf("%f %f %f\n",base[0]+vec[0],base[1]+vec[1],base[2]+vec[2]);
		printf("%f %f %f\n",base[0]+alt[0],base[1]+alt[1],base[2]+alt[2]);
		printf("%f %f %f\n\n",base[0],base[1],base[2]);
	}function triangle(){
		printf("#triangle\n");
		if (comment!="") printf("%s\n",comment);
		printf("%f %f %f\n",base[0],base[1],base[2]);
		printf("%f %f %f\n",base[0]+size[0],base[1]+size[1],base[2]+size[2]);
		printf("%f %f %f\n",base[0]+vec[0],base[1]+vec[1],base[2]+vec[2]);
		printf("%f %f %f\n\n",base[0],base[1],base[2]);
	}function away(){
		printf("#cube-away\n");
		a[0]=0;		a[1]=size[1];	a[2]=0;		b[0]=size[0];	b[1]=0;		b[2]=0;
		plane(base,a,b);
	}function home(){
		printf("#cube-home\n");
		a[0]=0;		a[1]=size[1];	a[2]=0;		b[0]=size[0];	b[1]=0;		b[2]=0;
		for(k=0;k<3;k++) shift[k]=base[k]+size[k]*(k==2);
		plane(shift,a,b);
	}function left(){
		printf("#cube-left\n");
		a[0]=0;		a[1]=size[1];	a[2]=0;		b[0]=0;		b[1]=0;		b[2]=size[2];
		plane(base,a,b);
	}function right(){
		printf("#cube-right\n");
		a[0]=0;		a[1]=size[1];	a[2]=0;		b[0]=0;		b[1]=0;		b[2]=size[2];
		for(k=0;k<3;k++) shift[k]=base[k]+size[k]*(k==0);
		plane(shift,a,b);
	}function ceil(){
		printf("#cube-ceil\n");
		a[0]=size[0];	a[1]=0;		a[2]=0;		b[0]=0;		b[1]=0;		b[2]=size[2];
		for(k=0;k<3;k++) shift[k]=base[k]+size[k]*(k==1);
		plane(shift,a,b);
	}function floor(){
		printf("#cube-floor\n");
		a[0]=size[0];	a[1]=0;		a[2]=0;		b[0]=0;		b[1]=0;		b[2]=size[2];
		plane(base,a,b);
	}{
		_sta=substr($0,1,1);
		if (_sta=="#")
		{
			if (comment=="") comment=$0;
			else comment=comment "\n" $0;
		}
		else if (NF==0)
		{
			if (comment!="") printf("%s\n",comment);
			comment="";
			printf("\n");
		}
		else {
			if ($4=="alt_vector")
			{
				alt[0]=$1;alt[1]=$2;alt[2]=$3;
			}
			else if ($4=="radius")
			{
				rad[0]=$1;rad[1]=$2;rad[2]=$3;
			}
			else if ($4=="theta")
			{
				theta[0]=$1;theta[1]=$2;theta[2]=$3;
			}
			else if ($4=="vector")
			{
				vec[0]=$1;vec[1]=$2;vec[2]=$3;
			}
			else if ($4=="width")
			{
				size[0]=$1;size[1]=$2;size[2]=$3;
			}
			else if ($4=="line")
			{
				#	Wx	Wy	Wz	width
				#	Ox	Oy	Oz	line
				#	draw line from (Ox,Oy,Oz) to (Ox+Wx,Oy+Wy,Oz+Wz)
				base[0]=$1;base[1]=$2;base[2]=$3;
				line();
				comment="";
			}
			else if ($4=="arc")
			{
				#	Rx	Ry	Rz	radius
				#	Vx	Vy	Vz	vector
				#	th1	th2	opt	theta
				#	Ox	Oy	Oz	arc
				#	draw arc at origin (Ox,Oy,Oz). Radius is defined by the size of (Rx,Ry,Rz)
				#	and normal is defined by (Rx,Ry,Rz)x(Vx,Vy,Vz).
				#	th1 and th2 is the begin-end angle starting from (Rx,Ry,Rz) in the direction
				#	of (Rx,Ry,Rz)x(Vx,Vy,Vz).
				#	* The size of vector (Vx,Vy,Vz) is just ignored
				base[0]=$1;base[1]=$2;base[2]=$3;
				arc();
				comment="";
			}
			else if ($4=="cube")
			{
				#	Wx	Wy	Wz	width
				#	Ox	Oy	Oz	cube
				#	draw cube from (Ox,Oy,Oz) to (Ox+Wx,Oy+Wy,Oz+Wz)
				base[0]=$1;base[1]=$2;base[2]=$3;
				left();
				right();
				ceil();
				floor();
				home();
				away();
				comment="";
			}
			else if ($4=="parallel")
			{
				#	Vx	Vy	Vz	vector
				#	Wx	Wy	Wz	width
				#	Ox	Oy	Oz	cube
				#	draw a parallelpiped normal to the direction where W*=0
				#	The base size is defined by other two W*.
				#	Side edge is defined by (Vx,Vy,Vz)
				base[0]=$1;base[1]=$2;base[2]=$3;
				if (size[0]==0)
				{
					a[0]=0;		a[1]=size[1];	a[2]=0;		b[0]=0;		b[1]=0;			b[2]=size[2];
					printf("#parallel-left\n");		plane(base,a,b);
					for(k=0;k<3;k++) shift[k]=base[k]+vec[k];
					printf("#parallel-right\n");	plane(shift,a,b);
					a[0]=vec[0];a[1]=vec[1];	a[2]=vec[2];b[0]=0;		b[1]=0;			b[2]=size[2];
					printf("#parallel-floor\n");	plane(base,a,b);
					for(k=0;k<3;k++) shift[k]=base[k]+size[k]*(k==1);
					printf("#parallel-ceil\n");		plane(shift,a,b);
					a[0]=vec[0];a[1]=vec[1];	a[2]=vec[2];b[0]=0;		b[1]=size[1];	b[2]=0;
					printf("#parallel-away\n");	plane(base,a,b);
					for(k=0;k<3;k++) shift[k]=base[k]+size[k]*(k==2);
					printf("#parallel-home\n");	plane(shift,a,b);
				}
				if (size[1]==0)
				{
					a[0]=size[0];	a[1]=0;		a[2]=0;			b[0]=0;			b[1]=0;			b[2]=size[2];
					printf("#parallel-floor\n");plane(base,a,b);
					for(k=0;k<3;k++) shift[k]=base[k]+vec[k];
					printf("#parallel-ceil\n");	plane(shift,a,b);
					a[0]=vec[0];a[1]=vec[1];	a[2]=vec[2];	b[0]=0;			b[1]=0;			b[2]=size[2];
					printf("#parallel-left\n");	plane(base,a,b);
					for(k=0;k<3;k++) shift[k]=base[k]+size[k]*(k==0);
					printf("#parallel-right\n");plane(shift,a,b);
					a[0]=vec[0];a[1]=vec[1];	a[2]=vec[2];	b[0]=size[0];	b[1]=0;			b[2]=0;
					printf("#parallel-away\n");	plane(base,a,b);
					for(k=0;k<3;k++) shift[k]=base[k]+size[k]*(k==2);
					printf("#parallel-home\n");	plane(shift,a,b);
				}
				if (size[2]==0)
				{
					a[0]=size[0];	a[1]=0;		a[2]=0;			b[0]=0;			b[1]=size[1];	b[2]=0;
					printf("#parallel-away\n");	plane(base,a,b);
					for(k=0;k<3;k++) shift[k]=base[k]+vec[k];
					printf("#parallel-home\n");	plane(shift,a,b);
					a[0]=vec[0];a[1]=vec[1];	a[2]=vec[2];	b[0]=0;			b[1]=size[1];	b[2]=0;
					printf("#parallel-left\n");	plane(base,a,b);
					for(k=0;k<3;k++) shift[k]=base[k]+size[k]*(k==0);
					printf("#parallel-right\n");plane(shift,a,b);
					a[0]=vec[0];a[1]=vec[1];	a[2]=vec[2];	b[0]=size[0];	b[1]=0;			b[2]=0;
					printf("#parallel-floor\n");plane(base,a,b);
					for(k=0;k<3;k++) shift[k]=base[k]+size[k]*(k==1);
					printf("#parallel-ceil\n");	plane(shift,a,b);
				}
			}
			else if ($4=="tetragon")
			{
				#	Wx	Wy	Wz	width
				#	Vx	Vy	Vz	vector
				#	Ax	Ay	Az	alt_vector
				#	Ox	Oy	Oz	tetragon
				#	draw tetragon (Ox,Oy,Oz) (Ox+Wx,Oy+Wy,Oz+Wz)
				#	(Ox+Vx,Oy+Vy,Oz+Vz) (Ox+Ax,Oy+Ay,Oz+Az) (Ox,Oy,Oz)
				#	Note: This draw a facet. If the four points are not on a single plane
				#		user must divide it into two triangles.
				base[0]=$1;base[1]=$2;base[2]=$3;
				tetragon();
				comment="";
			}
			else if ($4=="triangle")
			{
				#	Wx	Wy	Wz	width
				#	Vx	Vy	Vz	vector
				#	Ox	Oy	Oz	triangle
				#	draw triangle (Ox,Oy,Oz) (Ox+Wx,Oy+Wy,Oz+Wz) (Ox+Vx,Oy+Vy,Oz+Vz)
				base[0]=$1;base[1]=$2;base[2]=$3;
				triangle();
				comment="";
			}
			else
				print;
		}
	}'
}
geometry_projection(){
	geometry $1 $2|projection $1 $2
}
make_model(){
	#	Create 3D-model file
	#		make_model [x_rotation [y_rotation [z_rotation]]]
	#		Reads standard input and draw the objects in Sony PlayStation3 DAE file
	#		Example:
	#			3	2.4	2.2	width
	#			0	0	0	cube	#FF0000FF
	#			5	0	0	width
	#			0	0	0	line	#00FF00FF
	TMP_OFF=`mktemp -dt offXXXXX`
	if [ -f $TMP_OFF/0.off ];then
		rm -rf $TMP_OFF/*.off
	fi
	if [ -f $TMP_OFF/0.dae ];then
		rm -rf $TMP_OFF/*.dae
	fi
	gawk -v folder=$TMP_OFF --non-decimal-data 'BEGIN{
		objs=0;
		#width default
		size[0]=1; size[1]=1; size[2]=1;
		#vector default
		vec[0]=1; vec[1]=1; vec[2]=1;
		#antipode default
		ant[0]=0; ant[1]=0; ant[2]=0;
		#color default
		color[0]=255; color[1]=0; color[2]=0; color[3]=255;
		ID_F=-1+13;ID_C=+1+13;ID_L=-3+13;ID_R=+3+13;LD_A=-9+13;ID_H=+9+13;
		dark[ID_F]=0.8; dark[ID_C]=1.0; dark[ID_R]=0.9; dark[ID_L]=0.9; dark[ID_H]=0.85; dark[ID_A]=0.85;
		comment="";
	}function cube(fr,to,id,col){
		file=sprintf("%s/%d.off",folder,id);
		printf("OFF\n8 6 0\n")	>	file;
		printf("%f %f %f\n",fr[0],fr[1],fr[2])						>>	file;
		printf("%f %f %f\n",fr[0]+to[0],fr[1],fr[2])				>>	file;
		printf("%f %f %f\n",fr[0]+to[0],fr[1]+to[1],fr[2])			>>	file;
		printf("%f %f %f\n",fr[0],fr[1]+to[1],fr[2])				>>	file;
		printf("%f %f %f\n",fr[0],fr[1],fr[2]+to[2])				>>	file;
		printf("%f %f %f\n",fr[0]+to[0],fr[1],fr[2]+to[2])			>>	file;
		printf("%f %f %f\n",fr[0]+to[0],fr[1]+to[1],fr[2]+to[2])	>>	file;
		printf("%f %f %f\n",fr[0],fr[1]+to[1],fr[2]+to[2])			>>	file;
		printf("4 0 3 2 1 %d %d %d %d\n",col[0]*dark[ID_A],col[1]*dark[ID_A],col[2]*dark[ID_A],col[3])	>>	file;
		printf("4 1 5 4 0 %d %d %d %d\n",col[0]*dark[ID_F],col[1]*dark[ID_F],col[2]*dark[ID_F],col[3])	>>	file;
		printf("4 2 6 5 1 %d %d %d %d\n",col[0]*dark[ID_R],col[1]*dark[ID_R],col[2]*dark[ID_R],col[3])	>>	file;
		printf("4 4 5 6 7 %d %d %d %d\n",col[0]*dark[ID_H],col[1]*dark[ID_H],col[2]*dark[ID_H],col[3])	>>	file;
		printf("4 6 2 3 7 %d %d %d %d\n",col[0]*dark[ID_C],col[1]*dark[ID_C],col[2]*dark[ID_C],col[3])	>>	file;
		printf("4 0 4 7 3 %d %d %d %d\n",col[0]*dark[ID_L],col[1]*dark[ID_L],col[2]*dark[ID_L],col[3])	>>	file;
		return id+1;
	}function get_color(str,col){
		printf("%s\n",str)	> "debug.txt";
		if ( str == "")
		{
		}
		else if (substr(str,1,1)=="#")
		{
			col[0]=sprintf("%d","0x" substr(str,2,2));
			col[1]=sprintf("%d","0x" substr(str,4,2));
			col[2]=sprintf("%d","0x" substr(str,6,2));
			col[3]=sprintf("%d","0x" substr(str,8,2));
		}
	}function line(fr,to,id,col){
		file=sprintf("%s/%d.off",folder,id);
		printf("OFF\n2 1 0\n")	>	file;
		printf("%f %f %f\n",fr[0],fr[1],fr[2])						>>	file;
		printf("%f %f %f\n",fr[0]+to[0],fr[1]+to[1],fr[2]+to[2])	>>	file;
		printf("2 0 1 %d %d %d %d\n",col[0],col[1],col[2],col[3])	>>	file;
		return id+1;
	}function parallel(fr,to,vc,id,col){
		file=sprintf("%s/%d.off",folder,id);
		printf("PARALLEL %f %f %f\n",vc[0],vc[1],vc[2])	>	"debug.txt";
		printf("OFF\n8 6 0\n")	>	file;
		x=fr[0];y=fr[1];z=fr[2];
		printf("%f %f %f\n",x,y,z)						>>	file;
		if (to[0]!=0) x+=to[0]; else { x+=vc[0];y+=vc[1];z+=vc[2];};
		printf("%f %f %f\n",x,y,z)						>>	file;
		if (to[1]!=0) y+=to[1]; else { x+=vc[0];y+=vc[1];z+=vc[2];};
		printf("%f %f %f\n",x,y,z)						>>	file;
		if (to[0]!=0) x-=to[0]; else { x-=vc[0];y-=vc[1];z-=vc[2];};
		printf("%f %f %f\n",x,y,z)						>>	file;
		x=fr[0];y=fr[1];z=fr[2];
		if (to[2]!=0) z+=to[2]; else { x+=vc[0];y+=vc[1];z+=vc[2];};
		printf("%f %f %f\n",x,y,z)						>>	file;
		if (to[0]!=0) x+=to[0]; else { x+=vc[0];y+=vc[1];z+=vc[2];};
		printf("%f %f %f\n",x,y,z)						>>	file;
		if (to[1]!=0) y+=to[1]; else { x+=vc[0];y+=vc[1];z+=vc[2];};
		printf("%f %f %f\n",x,y,z)						>>	file;
		if (to[0]!=0) x-=to[0]; else { x-=vc[0];y-=vc[1];z-=vc[2];};
		printf("%f %f %f\n",x,y,z)						>>	file;
		printf("4 0 3 2 1 %d %d %d %d\n",col[0]*dark[ID_A],col[1]*dark[ID_A],col[2]*dark[ID_A],col[3])	>>	file;
		printf("4 1 5 4 0 %d %d %d %d\n",col[0]*dark[ID_F],col[1]*dark[ID_F],col[2]*dark[ID_F],col[3])	>>	file;
		printf("4 2 6 5 1 %d %d %d %d\n",col[0]*dark[ID_R],col[1]*dark[ID_R],col[2]*dark[ID_R],col[3])	>>	file;
		printf("4 4 5 6 7 %d %d %d %d\n",col[0]*dark[ID_H],col[1]*dark[ID_H],col[2]*dark[ID_H],col[3])	>>	file;
		printf("4 6 2 3 7 %d %d %d %d\n",col[0]*dark[ID_C],col[1]*dark[ID_C],col[2]*dark[ID_C],col[3])	>>	file;
		printf("4 0 4 7 3 %d %d %d %d\n",col[0]*dark[ID_L],col[1]*dark[ID_L],col[2]*dark[ID_L],col[3])	>>	file;
		return id+1;
	}function plane(fr,a,b,id,col){
		file=sprintf("%s/%d.off",folder,id);
		printf("OFF\n4 2 0\n")	>	file;
		x=fr[0];y=fr[1];z=fr[2];
		printf("%f %f %f\n",x,y,z)						>>	file;
		printf("%f %f %f\n",x+a[0],y+a[1],z+a[2])		>>	file;
		printf("%f %f %f\n",x+a[0]+b[0],y+a[1]+b[1],z+a[2]+b[2])		>>	file;
		printf("%f %f %f\n",x+b[0],y+b[1],z+b[2])		>>	file;
		printf("4 0 1 2 3 %d %d %d %d\n",col[0],col[1],col[2],col[3])	>>	file;
		printf("4 0 3 2 1 %d %d %d %d\n",col[0],col[1],col[2],col[3])	>>	file;
		return id+1;
	}function triangle(fr,a,b,id,col){
		file=sprintf("%s/%d.off",folder,id);
		printf("OFF\n3 2 0\n")	>	file;
		x=fr[0];y=fr[1];z=fr[2];
		printf("%f %f %f\n",x,y,z)						>>	file;
		printf("%f %f %f\n",x+a[0],y+a[1],z+a[2])		>>	file;
		printf("%f %f %f\n",x+b[0],y+b[1],z+b[2])		>>	file;
		printf("3  0 1 2 %d %d %d %d\n",col[0],col[1],col[2],col[3])	>>	file;
		printf("3  0 2 1 %d %d %d %d\n",col[0],col[1],col[2],col[3])	>>	file;
		return id+1;
	}{
		_sta=substr($0,1,1);
		if (_sta=="#")
		{
			if (comment=="") comment=$0;
			else comment=comment "\n" $0;
		}
		else if (NF==0)
		{
			if (comment!="") printf("%s\n",comment);
			comment="";
			printf("\n");
		}
		else {
			if ($4=="anti")
			{
				ant[0]=$1;ant[1]=$2;ant[2]=$3;
			}
			else if ($4=="cube")
			{
				#	Wx	Wy	Wz	width
				#	Ox	Oy	Oz	cube
				#	draw cube from (Ox,Oy,Oz) to (Ox+Wx,Oy+Wy,Oz+Wz)
				base[0]=$1;base[1]=$2;base[2]=$3;
				get_color($5,color);
				objs=cube(base,size,objs,color);
				comment="";
			}
			else if ($4=="line")
			{
				#	Wx	Wy	Wz	width
				#	Ox	Oy	Oz	line	[color]
				#	draw line from (Ox,Oy,Oz) to (Ox+Wx,Oy+Wy,Oz+Wz)
				base[0]=$1;base[1]=$2;base[2]=$3;
				get_color($5,color);
				objs=line(base,size,objs,color);
				comment="";
			}
			else if ($4=="parallel")
			{
				#	Vx	Vy	Vz	vector
				#	Wx	Wy	Wz	width
				#	Ox	Oy	Oz	parallel	[color]
				#	draw a parallelpiped normal to the direction where W*=0
				#	The base size is defined by other two W*.
				#	Side edge is defined by (Vx,Vy,Vz)
				base[0]=$1;base[1]=$2;base[2]=$3;
				get_color($5,color);
				objs=parallel(base,size,vec,objs,color);
				comment="";
			}
			else if ($4=="plane")
			{
				#	Vx	Vy	Vz	vector
				#	Wx	Wy	Wz	width
				#	Ox	Oy	Oz	quad	[color]
				base[0]=$1;base[1]=$2;base[2]=$3;
				get_color($5,color);
				objs=plane(base,size,vec,objs,color);
				comment="";
			}
			else if ($4=="triangle")
			{
				#	Vx	Vy	Vz	vector
				#	Wx	Wy	Wz	width
				#	Ox	Oy	Oz	triangle	[color]
				base[0]=$1;base[1]=$2;base[2]=$3;
				get_color($5,color);
				objs=triangle(base,size,vec,objs,color);
				comment="";
			}
			else if ($4=="vector")
			{
				vec[0]=$1;vec[1]=$2;vec[2]=$3;
			}
			else if ($4=="width")
			{
				size[0]=$1;size[1]=$2;size[2]=$3;
			}
			else
				print;
		}
	}'
	#make objects into one
	off_util $TMP_OFF/*.off	> $TMP_OFF/step1.off
	#rotate it
	off_trans	-R ${1:-0},${2:-0},${3:-0}	$TMP_OFF/step1.off	>	$TMP_OFF/step2.off
	#make PS3-DAE (omit vertices by -x v, omit edges by -x e)
	off2dae	-x v < $TMP_OFF/step2.off
}
make_sample(){
#----------------------------------------------------
#	EXAMPLE 1   Create PDF
#----------------------------------------------------
cat>	ex_1.sh	<<%
#!/bin/bash
. draw.sh
. object.sh

TMP=`mktemp -dt objXXXXX`

COLOR=1

X_RANGE='-3 3'
Y_RANGE='-3 3'
X_AXIS=''
Y_AXIS=''
COLOR=1
LINE=( Solid:Black:0.00001 )
SYMBOL=( Omit )
OPT='-q 0.8 -N x -N y --pen-colors=2=DarkGreen:4=#4B0082:5=#EFBF32:6=#4682B4'
cat>\$TMP/legend<<@
@

input(){
cat<<@
0.5	0	0	radius
0	0	1	vector
0	90	0	theta
#arc-1
#m=5,S=0
-2.0	0	0	arc
0	-0.5	0	radius
0	90	0	theta
#arc-2
#m=1,S=0
-2.0	0	0	arc
1	0	0	vector
#arc-3
#m=2,S=0
0	-0.5	0	radius
-2.0	0	0	arc

#m=1,S=0
18		0	0	width
-4		0	0	line
#m=2,S=0
0	18	0	width
0	-4	0	line
#m=3,S=0
0	0	18	width
0	0	-4	line
#cubes
1	5	1	width
#m=4,S=0
-0.5	-0.5	-0.5	cube
0.25	2.25	0.25	width
#m=1,S=0
-0.125	-2.0	-1.625	cube
0	5	1	width
0.5	0.8	0.3	vector
#m=6,S=0
0.75	-0.5	-0.5	parallel
0.5	5.8	0.1	vector
1	1	0	width
#m=6,S=0
-.5	-0.5	-1.0	parallel

#arc-4
#m=3,S=0
1	0	0	vector
0	-0.75	0	radius
90	360	0	theta
-2.0	0	-0.02	arc

#m=1,S=0
0.0001	4	4	width
1.9	-2	-2	cube
#m=3,S=0
0.01	1	1	width
1.89	-0.5	-0.5	cube

@
}
input2(){
cat<<@
#m=1,S=0
1	1	1	width
1	-1	1	vector
0	0	0	triangle

@
}
make_pdf(){
	echo "Starting Example1. It takes 2 minutes to create PDF file"
	geometry_projection 260 85 > \$1.obj
	echo "Created \$1.obj"
	draw \$1.pdf \$1.obj
	echo "Created \$1.pdf"
}
input2|make_pdf ex_1
#----------------------------------------------------
#	EXAMPLE 2   Create DAE
#----------------------------------------------------
off_input(){
cat<<@
#X-axis
3	0	0	width
0	0	0	line	#FF0000FF
#Y-axis
0	3	0	width
0	0	0	line	#00FF00FF
#Z-axis
0	0	3	width
0	0	0	line	#0000FFFF
#Cube
1	1	1	width
0		0		0		cube	#FF0000FC
-0.5	-0.5	-0.5	cube	#00FF00FC
0	4	4	width
0		-2		-2		cube	#333333F0
#Parallel
1	1	0	width
-0.5		1		1		parallel	#0077FFFC
3	1	0.5	vector
0	0.25	0.25	width
-0.75		-0.5		0.1		parallel	#FF77FFFC
#Triangle
1	1	0	width
1	0	1	vector
1	1	1	triangle	#CC33CCFC
#Plane
1.5	1	1	plane	#FF9900FC
@
}
echo "Starting Example2. It creates DAE (Sony Playstation3 format) file"
off_input|make_model 0 0 30 > ex1.dae
echo "Created ex1.dae, which can be displayed by Mac Preview.app"
%
echo "Created sample script ex_1.sh"
chmod +x ex_1.sh
}




#---------------------------------------
if [ `basename $0` == "object.sh" ];then
	make_sample
fi
